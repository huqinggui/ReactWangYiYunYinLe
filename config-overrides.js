const path = require( 'path' )
const { override, fixBabelImports,addWebpackAlias } = require('customize-cra')
module.exports = override(
       fixBabelImports('import', {
         libraryName: 'antd-mobile',
         style: true,
       }),
       addWebpackAlias({
        // [路径名称]: 磁盘路径
        ['@']: path.join( __dirname, 'src' ),
        ['pages']: path.join( __dirname, 'src/component/pages' ),
        ['common']: path.join( __dirname, 'src/component/common' ),
        ['lib']: path.join( __dirname, 'src/lib' ),
        ['store']: path.join( __dirname, 'src/store' ),
        ['assets']: path.join( __dirname, 'src/assets' ),
        ['utils']: path.join( __dirname, 'src/utils' ),
        ['mock']: path.join( __dirname, 'src/mock' )
      })
     );