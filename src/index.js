import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
//引入重置样式
import 'assets/css/reset.css'
//引入自适应rem文件
import 'utils/rem.js'
//引入abtd-design ui组件库样式
import 'antd-mobile/dist/antd-mobile.css'
//引入字体图标
import 'assets/font/iconfont.css'
//引入history路由
import { BrowserRouter as Router} from 'react-router-dom'
//引入store
import store from './store'
//引入store包裹组件
import { Provider } from 'react-redux'
//引入toast亲提示,用于axios拦截器
import { Toast } from 'antd-mobile';
//引入axios
import axios from 'axios'

//axios拦截器
axios.interceptors.request.use(function (config) {
    // 请求数据的时候做的事
    Toast.loading('Loading...', 1, () => {
        console.log('Load complete !!!');
      });
    return config;
  }, function (error) {
    // 请求数据出错
    return Promise.reject(error);
  });
 
// 请求到数据的时候
axios.interceptors.response.use(function (response) {
    //请求到数据的时候做的事
    Toast.hide();
    return response;
  }, function (error) {
    // 请求到数据出错
    return Promise.reject(error);
  });

ReactDOM.render(
    <Provider store = { store }>
        <Router>
            <App />
        </Router>
    </Provider>,
      document.getElementById('root')
    );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
