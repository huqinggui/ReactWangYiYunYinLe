import React from 'react';
import './App.css';
import Home from 'pages/home'
import { Route,Redirect,withRouter } from 'react-router-dom'
import Recommend from 'pages/home/show/recommend'
import Rank from 'pages/home/show/rank'
import Singer from 'pages/home/show/singer'
import Player from 'pages/home/play'

class App extends React.Component {
  render () {
    return (
        <div className="App">
            <Home></Home>
            <Route  exact path = "/" render = { () => (<Redirect to = '/recommend'></Redirect>) }></Route>
            <Route path = "/recommend" component = { Recommend }></Route>
            <Route path = "/rank" component ={ Rank }></Route>
            <Route path = "/singer" component ={ Singer }></Route>
            <Player></Player>
        </div>
      )
  }
  
}

export default withRouter(App);
