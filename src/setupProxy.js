const proxy = require( 'http-proxy-middleware' )

module.exports = function ( app ) {
  // app.use( 反向代理标识，配置项)
  app.use( proxy('/wangyi', {
    target: 'http://localhost:4000',
    changeOrigin: true,
    pathRewrite: {
      '^/wangyi': ''
    }
  }))
}