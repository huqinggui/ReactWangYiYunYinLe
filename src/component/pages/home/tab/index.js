import React,{ Component,Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import './index.scss'

class Tab extends Component {
    render () {
        return (
            <Fragment>
                <div className="tab">
                    <div className="tab-item">
                        {/* <a className="active">推荐</a> */}
                        <NavLink activeClassName="active" to = {{ pathname : "/recommend" }}>推荐</NavLink>
                    </div>
                    <div className="tab-item">
                        {/* <a>排行</a> */}
                        <NavLink activeClassName="active" to = {{ pathname : "/rank" }}>排行</NavLink>
                    </div>
                    <div className="tab-item">
                        {/* <a>歌手</a> */}
                        <NavLink activeClassName="active" to = {{ pathname : "/singer" }}>歌手</NavLink>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default Tab