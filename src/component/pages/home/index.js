import React,{ Component,Fragment } from 'react'
import WHead from 'common/header'
import Tab from 'pages/home/tab'
import Show from 'pages/home/show'

class Home extends Component {
    render () {
        return (
            <Fragment>
                <WHead></WHead>
                <Tab></Tab>
                <Show></Show>
            </Fragment>
        )
    }
}

export default Home