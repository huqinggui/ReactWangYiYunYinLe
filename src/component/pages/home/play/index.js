import React,{ Component,Fragment } from 'react'
import ReactPlayer from 'react-player'
import Duration from 'utils/Duration'
import getSongInfoStore from 'utils/get_store/playinfo_store.js'
import BScroll from 'better-scroll'
import './index.scss'

const LyricItem = (props) => {
    return(
        <p className={props.format() > props.item.slice(1,6) ? 'lyric__para current' : 'lyric__para' }> { props.item.slice(11) }</p>
    )
}

class Play extends Component {
    constructor () {
        super()
        this.state = {
            url: '',//视频地址
            songReady: false,//歌曲是否准备好
            lyricFlag : true,//歌词是否显示
            currentTime: 0,//进度条
            duration: 0,//总时长
            playing: true,//是否播放
            played: 0,//和播放时间有关
            seeking: false,
            loop: false, //是否循环
            lyricList : [],//歌词列表
            index : Number
        }
    }

    async componentDidMount () {
        new BScroll(this.lyric,{
            scrollY: true,
            click: true
        })
        this.setState({
            index : this.props.playInfo.playList.index && Number(this.props.playInfo.playList.index)
        })
        
    }

    // componentWillReceiveProps (props) {
    //     if(this.props.playInfo.playFlag){
    //         this.props.getLyric({
    //             id : props.playInfo.playList.index && props.playInfo.playList.list[props.playInfo.playList.index].id
    //         })
    //     }
    // }
    
    closeNormalPlayer = () => {//改变play的隐藏
        this.props.changePlayFlag()
    }
    playPause = () => { //控制播放开关
        this.setState({ playing: !this.state.playing })
    }
    onDuration = (duration) => {//和时长有关
        console.log('onDuration', duration)
        this.setState({ duration })
    }
    onSeekMouseDown = e => {//按下
        this.setState({ seeking: true })
    }
    onSeekChange = e => {//拖动
        this.setState({ played: parseFloat(e.target.value) })
    }
    onSeekMouseUp = e => {//弹起
        this.setState({ seeking: false })
        this.player.seekTo(parseFloat(e.target.value))
    }
    toggleLoop = () => {
        this.setState({ loop: !this.state.loop })
    }

    onProgress = state => {
        console.log('onProgress', state)
        // We only want to update time slider if we are not currently seeking
        if (!this.state.seeking) {
          this.setState(state)
        }
    }
    onEnded = () => {//播放结束
        console.log('onEnded')
        
        if(this.state.loop){
            this.setState({ playing: this.state.loop })
        }
        else{
            this.props.changeIndex(Number(this.state.index) + 1)
        }
    }

    showLyric = () => {//展示歌词
        this.setState({
            lyric : !this.state.lyric
        })
    }

    format = (interval) => {//格式化时间
        interval = this.state.playedSeconds | 0
        let minute = interval / 60 | 0
        let second = interval % 60
        if (second < 10) {
          second = '0' + second
        }
        if(minute < 10){
            minute = '0' + minute
        }
        return minute + ':' + second
      }

      prevSong = () => {//上一首
        let i = Number(this.props.playInfo.playList.index)
        if(i < 1 ){
            this.props.changeIndex(0)
            this.setState({
                index : this.props.playInfo.playList.length
            })
            }else{
                    this.props.changeIndex(i - 1)
                    this.setState({
                    index :i - 1
                }) 
                this.props.getLyric({
                    id : this.props.playInfo.playList.list[this.props.playInfo.playList.index] && this.props.playInfo.playList.list[this.props.playInfo.playList.index].id
                })
            }
      }

      nextSong = () => {//下一首
        let i = Number(this.props.playInfo.playList.index)
        if(i > this.props.playInfo.playList.length - 2 ){
        this.props.changeIndex(0)
        this.setState({
            index :0
        })
        }else{
            this.props.changeIndex(i + 1)
            this.setState({
            index :i + 1
            }) 
            this.props.getLyric({
                id : this.props.playInfo.playList.list[this.props.playInfo.playList.index] && this.props.playInfo.playList.list[this.props.playInfo.playList.index].id
            })
        }
      }

    renderLyricItem = () => {
        return Boolean(this.props.playInfo.lyric) &&  this.props.playInfo.lyric.lrc.lyric.split('\n').map( (item,index) => {
            return <LyricItem format = { this.format } item = { item } key = { index } index = { index }></LyricItem>
        })
    }

    render () {
        let { playing,duration,played,loop,lyric,lyricList } = this.state
        return (
            <Fragment>
                <ReactPlayer ref = { el => this.player = el } loop={ loop } onSeek={e => console.log('onSeek', e)}  onProgress={ this.onProgress } playing = { playing } onDuration={ this.onDuration } onEnded={ this.onEnded } url={`https://music.163.com/song/media/outer/url?id=${ this.props.playInfo.playList.list[this.props.playInfo.playList.index] && this.props.playInfo.playList.list[this.props.playInfo.playList.index].id }.mp3`}/>
                <div className="player">
                    <div ref = { el => this.normalPlayer = el } className="normal-player" style = {{ display : this.props.playInfo.playFlag ? 'block' : 'none'}}>
                        <div className="background">
                            <div className="filter"></div> 
                            <img alt="" width="100%" height="100%" src={ this.props.playInfo.playList.imgs && this.props.playInfo.playList.imgs[this.props.playInfo.playList.index] }/>
                        </div>
                        <div className="top">
                            <div className="back" onClick = { this.closeNormalPlayer }>
                                <i className="iconfont icon-xiala"></i>
                            </div> 
                            <h1 className="title">{ this.props.playInfo.playList.list[this.props.playInfo.playList.index] && this.props.playInfo.playList.title[this.props.playInfo.playList.index] }</h1> 
                            <h2 className="subtitle">{ this.props.playInfo.playList.list[this.props.playInfo.playList.index] && this.props.playInfo.playList.name[this.props.playInfo.playList.index] }</h2>
                        </div>
                        <div className="middle">
                            <div className="middle-l" onClick = { this.showLyric } style = {{ display : !lyric ? 'block' : 'none'}}>
                                <div className="cd-wrapper">
                                    <div className={ playing ? 'cd play' : 'cd play pause' }>
                                    <img alt="" className="image" src={ this.props.playInfo.playList.imgs && this.props.playInfo.playList.imgs[this.props.playInfo.playList.index] }/>
                                    </div>
                                </div>
                            </div>
                            <div className="middle-r" ref = { el => this.lyric = el } onClick = { this.showLyric } style = {{ display : lyric ? 'block' : 'none'}}>
                                <div className="lyric-wrapper">
                                    { Boolean(this.props.playInfo.lyric) && this.renderLyricItem() }
                                    { !this.props.playInfo.lyric || <p className="no-lyric">暂无歌词</p> }
                                </div>
                            </div>
                        </div>
                        <div className="bottom">
                            <div className="progress-wrapper">
                                <span className="time time-l"><Duration seconds={duration * played}></Duration></span>
                                <div className="progress-bar-wrapper">
                                    <div className="progress-bar">
                                        <div className="bar-inner">
                                            {/* { (played/duration) * 1500} */}
                                        <progress className="progress" max="1" value={played}></progress>
                                            {/* <div className="progress"></div>  */}
                                        <input className="progress" id = "range" type="range" step="any" min="0" max="1" value={ played }
                                        onMouseDown={this.onSeekMouseDown}
                                        onChange={this.onSeekChange}
                                        onMouseUp={this.onSeekMouseUp}
                                        onTouchStart={this.onSeekMouseDown}
                                        onTouchMove={this.onSeekChange}
                                        onTouchEnd={this.onSeekMouseUp}
                                        />
                                            {/* <div className="progress-btn-wrapper" style={{transform: `translate3d(126.936px, 0px, 0px)`}}>
                                                <div className="progress-btn">
                                                </div>
                                            </div> */}
                                        </div>
                                    </div>
                                </div>
                                <span className="time time-r"><Duration seconds={ duration } /></span>

                            </div>
                            <div className="operators">
                                <div className="icon i-left">
                                        { !loop ? <i className="iconfont icon-liebiaoxunhuan" onClick = { this.toggleLoop }></i> : <i className="iconfont icon-danquxunhuan" onClick = { this.toggleLoop }></i> }
                                </div> 
                                <div className="icon i-left">
                                    <i className="iconfont icon-047caozuo_shangyishou" onClick = { this.prevSong }></i>
                                </div> 
                                <div className="icon i-center">
                                    { playing ? <i className="iconfont icon-zanting"  onClick = { this.playPause }></i> : <i className="iconfont mode icon-bofang1"  onClick = { this.playPause }></i>}
                                    {/* <i className="iconfont mode icon-bofang1"></i> */}
                                </div>
                                <div className="icon i-right">
                                    <i className="iconfont icon-49xiayishou" onClick = { this.nextSong }></i>
                                </div> 
                                <div className="icon i-right">
                                    <i className="iconfont icon-shoucang"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </Fragment>
        )
    }
}


export default getSongInfoStore({
    UIComponent: Play
})