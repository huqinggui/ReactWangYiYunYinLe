import React,{ Component,Fragment } from 'react'
import BScroll from 'better-scroll'
import Banner from './banner'
import RecoSongList from './reco_gedanlist'
import SongList from './reco_tuijiansong'
import { Route } from 'react-router-dom'
import RankDetail from './rank_detail'

import './index.scss'

class Recommend extends Component {

    componentDidMount () {
        new BScroll(this.scrollBox,{
            scrollY: true,
            click: true
        })
    }
    render () {
        return (
            <Fragment>
                <div className="recommend" ref = { el => this.scrollBox = el}>
                    <div className="recommend-content">
                        <Banner></Banner>
                        <RecoSongList></RecoSongList>
                        <SongList></SongList>
                    </div>
                </div>
                <Route react path = "/recommend/:id" component = { RankDetail }></Route>
            </Fragment>
        )
    }
}

export default Recommend