import React,{ Component,Fragment } from 'react'
import { NavLink } from 'react-router-dom'
import getRecommendStore from 'utils/get_store/recommend_store.js'
import './index.scss'

const PersonalizedItem = (props) => {
    return (
        <li className="item">
            <NavLink onClick = { props.count } to = {{ pathname : `/recommend/${ props.item.id }` }}>
                <div className="icon">
                    <div className="gradients"></div>
                    <img alt = { props.item.name } src = { props.item.picUrl }></img>
                </div>
                <p className="play-count">
                    <i className="iconfont icon-tubiaozhizuomobanyihuifu-"></i>
                    { props.playCount(props.item.playCount) }
                </p>
                <div className="text">
                    <p className="name">{ props.item.name }</p>
                </div>
            </NavLink>
        </li>
    )
}
class RecoSongList extends Component {

    async componentDidMount () {
        await this.props.getPersonalized()
    }

    playCount = (count) => {//对后台数据进行取万操作
        if(count>10000){
            return (Math.floor(count/10000) + '万')
        }
    }

    count = () => {
        console.log('路由触发点击事件啦~~~')
    }

    renderPersonalizedItem = () => {
        return this.props.recommend.personalized && this.props.recommend.personalized.slice(0,9).map( (item,index) => {
            return <PersonalizedItem count = { this.count } playCount ={ this.playCount } item = { item } key = { index }></PersonalizedItem>
        })
    }
    render () {
        return (
            <Fragment>
                <div className="recommend-list">
                    <h1 className="title">推荐歌单</h1>
                    <ul>
                        { this.renderPersonalizedItem() }
                    </ul>
                </div>
            </Fragment>
        )
    }
}

export default getRecommendStore({
    UIComponent: RecoSongList
})