import React,{ Component,Fragment } from 'react'
import BScroll from 'better-scroll'
import getRankStore from 'utils/get_store/rank_store.js'
import './index.scss'

const LiItem = (props) => {
    return (
        <li className="item" onClick = {  props.getPlayList.bind(this,props.index,props.item.id) }>
            <p className="count">{ props.count + 1 }</p>
            <div className="content">
                <h2 className="name">{ props.item.name }</h2> 
                <p className="desc">{ props.item.ar[0].name }</p>
            </div>
        </li>
    )
}

class RankDetail extends Component {

    async componentDidMount () {
        let search = window.location.href.slice(32)//获取id参数

        await this.props.getTopListDetail({
            id : search
        })
        let bscroll = new BScroll(this.scrollBox,{//滚动实例化
            scrollY: true,
            click: true,
            probeType : 2,
        })
        let num = 0;
        bscroll.on('scroll', (pos) => {//滚动监听,需要probeType : 2
            if(pos.y<-400){
                num += 0.03
                this.header.style.background = `rgba(212, 68, 57, ${num})`
            }else{
                this.header.style.background = `rgba(212, 68, 57, 0)`
            }
            if(pos.y<-500){
                this.title.innerText = this.props.rank.topListDetail.name && this.props.rank.topListDetail.name
            }else{
                this.title.innerText = '歌单'
            }
        })
    }

    goRank = () => {//返回按钮
        this.props.history.push('/recommend')
    }

    changeTime = (time) => {//时间格式化
        let mytime = new Date(time)
        return mytime.getMonth() + 1 + '月' + mytime.getDate() + '日'
    }

    playCount = (count) => {//对后台数据进行取万操作
        if(count>10000){
            return (Math.floor(count/10000) + '万')
        }
    }
    getPlayList = (index,id) => {//播放目录
        let imgarr = []
        let titlearr = []
        let namearr = []
        this.props.rank.topListDetail.tracks && this.props.rank.topListDetail.tracks.map( (item,index) => {
            imgarr.push(item.al.picUrl)
            titlearr.push(item.name)
            namearr.push(item.al.name)
        })
        
        this.props.getPlayList({
            arr : this.props.rank.topListDetail.tracks && this.props.rank.topListDetail.tracks,
            index : index,
            imgs : this.props.rank.topListDetail.tracks && imgarr,
            title : this.props.rank.topListDetail.tracks && titlearr,
            name : this.props.rank.topListDetail.tracks && namearr
        })
        // this.props.getSongInfo({
        //     id :  this.props.playInfo.playList.list && this.props.playInfo.playList.list[index].id
        // })
        this.props.getLyric({
            id : id
        })
    }

    renderLiItem = () => {
        return this.props.rank.topListDetail.tracks && this.props.rank.topListDetail.tracks.map( (item,index) => {
            return <LiItem getPlayList = { this.getPlayList } index = { index } item = { item } count = { index } key = { index }></LiItem>
        })
    }
    render () {
        
        return (
            <Fragment>
                <div className="music-list">
                    <div className="header" ref = { el => this.header = el }>
                        <div className="back" onClick = { this.goRank }>
                            <i className="iconfont icon-fanhui"></i>
                        </div> 
                        <div className="text">
                            <h1 className="title" ref = { el => this.title = el }>歌单</h1>
                        </div>
                    </div>
                    <div className="list" ref = { el => this.scrollBox = el}>
                        <div className="scroll">
                            <div className="bg-image" style={{ background: `url(${ this.props.rank.topListDetail.coverImgUrl && this.props.rank.topListDetail.coverImgUrl }) center center / cover no-repeat` }}>
                                <div className="filter"></div> 
                                <div className="text">
                                    <h2 className="list-title">
                                    { this.props.rank.topListDetail.name && this.props.rank.topListDetail.name }
                                    </h2> 
                                    {/* <p className="update">最近更新:{ this.props.rank.topListDetail.updateTime && this.changeTime(this.props.rank.topListDetail.updateTime) }</p> */}
                                    <p className="play-count">
                                        <i className="iconfont icon-tubiaozhizuomobanyihuifu-"></i>
                                        { this.props.rank.topListDetail.playCount && this.playCount( this.props.rank.topListDetail.playCount ) }
                                    </p>
                                </div>
                            </div>
                            <div className="song-list-wrapper">
                                <div className="sequence-play">
                                    <i className="iconfont icon-bofang"></i> 
                                    <span className="text">播放全部</span> 
                                    <span className="count">({ this.props.rank.topListDetail.tracks && this.props.rank.topListDetail.tracks.length })</span>
                                </div>
                                <div className="song-list">
                                    <ul>
                                        { this.renderLiItem() }
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default getRankStore({
    UIComponent: RankDetail
})