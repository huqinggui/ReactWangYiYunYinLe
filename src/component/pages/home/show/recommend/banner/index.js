import React,{ Component,Fragment } from 'react'
import Swiper from 'swiper'
import getRecommendStore from 'utils/get_store/recommend_store.js'
import 'swiper/dist/css/swiper.css'

import './index.scss'

const BannerItem = (props) => {
    return (
        <div className="swiper-slide"><img alt="" src = { props.item.imageUrl }/></div>
    ) 
}

class Banner extends Component {

    componentDidMount () {
        this.props.getBannerList()//请求banner数据
        setTimeout( () => {//swiper实例化
            this.swiper = new Swiper (this.bannerList, {
                loop: true,// 循环模式选项
                pagination: {// 如果需要分页器
                    el: '.swiper-pagination',
                },
                autoplay: {//自动轮播配置
                    delay: 1500,
                    stopOnLastSlide: false,
                    disableOnInteraction: true,
                }
              })        
        },1000)
    }

    renderBannerItem = () => {
        return this.props.recommend.bannerList && this.props.recommend.bannerList.slice(0,5).map( (item,index) => {
            return <BannerItem item = { item } key = { index }></BannerItem>
        })
    }

    render () {
        return (
            <Fragment>
                <div className="red_bg"></div>
                <div className="swiper-container" ref = { el => this.bannerList = el}>
                    <div className="swiper-wrapper">
                        { this.renderBannerItem() }
                    </div>
                    {/* <!-- 如果需要分页器 --> */}
                    <div className="swiper-pagination"></div>
                </div>
            </Fragment>
        )
    }
}
export default getRecommendStore({
    UIComponent: Banner
})