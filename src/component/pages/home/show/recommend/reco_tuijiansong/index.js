import React,{ Component,Fragment } from 'react'
import getRecommendStore from 'utils/get_store/recommend_store.js'
import './index.scss'

const SongListItem = (props) => {
    return (
        <li className="item" onClick = { props.getPlayList.bind(this,props.index,props.item.id) }>
            <div className="icon">
                <div className="gradients"></div>
                <img alt = { props.item.name } src = { props.item.song.album.picUrl }></img>
            </div>
            <div className="text">
                <p className="name">{ props.item.name }</p>
            </div>
            <div className="singer">{ props.item.song.artists[0].name }</div>
        </li> 
    )
}

class SongList extends Component {

    async componentDidMount () {
        await this.props.getTuiJianSong()  
    }

    getPlayList = (index,id) => {//播放目录
        let imgarr = []
        let titlearr = []
        let namearr = []
        this.props.recommend.tuijianSong && this.props.recommend.tuijianSong.map( (item,index) => {
            imgarr.push(item.song.album.picUrl)
            titlearr.push(item.name)
            namearr.push(item.song.artists[0].name)
        })
        
        this.props.getPlayList({
            arr : this.props.recommend.tuijianSong && this.props.recommend.tuijianSong,
            index : index,
            imgs : this.props.recommend.tuijianSong && imgarr,
            title : this.props.recommend.tuijianSong && titlearr,
            name : this.props.recommend.tuijianSong && namearr
        })
        // this.props.getSongInfo({
        //     id :  this.props.playInfo.playList.list && this.props.playInfo.playList.list[index].id
        // })
        this.props.getLyric({
            id : id
        })
    }

    renderSongListItem = () => {
        return this.props.recommend.tuijianSong && this.props.recommend.tuijianSong.slice(0,9).map( (item,index) => {
            return <SongListItem getPlayList = { this.getPlayList } index = { index } item = { item } key = { index }></SongListItem>
        })
    }

    render () {
        return(
            <Fragment>
                <div className="recommend-song">
                    <h1 className="title">推荐歌曲</h1>
                    <ul>
                        { this.renderSongListItem() }
                    </ul>
                </div>
            </Fragment>
        )
    }
}

export default getRecommendStore({
    UIComponent: SongList
})