import React,{ Component,Fragment } from 'react'
import { NavLink } from 'react-router-dom'

class StatePower extends Component {

    render () {
        return (
            <Fragment>
                 <NavLink to = {{ pathname : `/rank/${ this.props.item && this.props.item.id }` }}>
                <li className="item">
                    <div className="icon">
                        <img alt = { this.props.item && this.props.item.description } src = { this.props.item && this.props.item.coverImgUrl }/>
                    </div>
                    <ul className="songlist">
                        <li className="song">
                            <span>1.</span>
                            <span>{ this.props.item && this.props.item.tracks[0].name } - { this.props.item && this.props.item.tracks[0].ar[0].name }</span>
                        </li>
                        <li className="song">
                            <span>2.</span>
                            <span>{ this.props.item && this.props.item.tracks[1].name } - { this.props.item && this.props.item.tracks[1].ar[0].name }</span>
                        </li>
                        <li className="song">
                            <span>3.</span>
                            <span>{ this.props.item && this.props.item.tracks[2].name } - { this.props.item && this.props.item.tracks[2].ar[0].name }</span>
                        </li>
                    </ul>
                </li>
                </NavLink>
            </Fragment>
        )
    }
}

export default StatePower