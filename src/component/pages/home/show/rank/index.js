import React,{ Component,Fragment } from 'react'
import BScroll from 'better-scroll'
import { Route } from 'react-router-dom'
import getRankStore from 'utils/get_store/rank_store.js'
import Hot from './hotlist'
import Speed from './speedlist'
import Orginal from './origin'
import Music from './music'
import StatePower from './statepower'
import RankDetail from './rankdetail'
import NewSong from './newsong';
import './index.scss'
/**
 * 此处遍历双层遍历
 * 待优化(可以将接口参数放在数组里)
 */
class Rank extends Component {

    componentDidMount () {
        if(window.location.href === 'http://localhost:3000/rank'){
            this.props.getTopList0({//新歌绑
                idx : 0
            })
            this.props.getTopList1({//热歌绑
                idx : 1
            })
            this.props.getTopList2({//原创绑
                idx : 2
            })
            this.props.getTopList3({//飙升绑
                idx : 3
            })
            this.props.getTopList4({//飙升绑
                idx : 4
            })
            this.props.getTopList22({//AGG音乐绑
                idx : 22
            })

        }
        // this.props.getTopListDetail()
        new BScroll(this.rankScroll,{//滚动实例化
            scrollY: true,
            click: true
        })
    }

    render () {
        return (
            <Fragment>
                <div className="rank" ref = { el => this.rankScroll = el}>
                    <div className="toplist">
                        <ul>
                            {/* 优化:避免结构先加载出来,数据还没出来 */}
                            { this.props.rank.topList0.playlist && <NewSong item = { this.props.rank.topList0 && this.props.rank.topList0.playlist }></NewSong> }
                            { this.props.rank.topList1.playlist && <Hot item = { this.props.rank.topList1 && this.props.rank.topList1.playlist }></Hot> }
                            { this.props.rank.topList3.playlist && <Speed item = { this.props.rank.topList3 && this.props.rank.topList3.playlist }></Speed> }
                            { this.props.rank.topList4.playlist && <StatePower item = { this.props.rank.topList4 && this.props.rank.topList4.playlist }></StatePower> }
                            { this.props.rank.topList2.playlist && <Orginal item = { this.props.rank.topList2 && this.props.rank.topList2.playlist }></Orginal> }
                            { this.props.rank.topList22.playlist && <Music item = { this.props.rank.topList22 && this.props.rank.topList22.playlist }></Music> }
                        </ul>
                    </div>
                </div>
                <Route path = '/rank/:id' component = { RankDetail }></Route>
            </Fragment>
        )
    }
}

export default getRankStore({
    UIComponent: Rank
})