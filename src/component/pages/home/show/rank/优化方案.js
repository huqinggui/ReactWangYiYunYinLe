import React,{ Component,Fragment } from 'react'
import BScroll from 'better-scroll'
import getRankStore from 'utils/get_store/rank_store.js'
import Hot from './hotlist'
import Speed from './speedlist'
import Orginal from './origin'
import Music from './music'
import StatePower from './statepower'
import './index.scss'
import NewSong from './newsong';
/**
 * 此处遍历双层遍历
 * 待优化(可以将接口参数放在数组里)
 * 优化方案循环使用
 */

const TopListItem = (props) => {
    return (
        <li className="item">
            <div className="icon">
                <img src="" alt=""/>
            </div>
            <ul className="songlist">
                <li className="song">
                    <span>1.</span>
                    <span>Hard Down - CROD</span>
                </li>
                <li className="song">
                    <span>2.</span>
                    <span>Hard Down - CROD</span>
                </li>
                <li className="song">
                    <span>3.</span>
                    <span>Hard Down - CROD</span>
                </li>
            </ul>
        </li>
    )
}

class Rank extends Component {
    constructor () {
        super ()
        this.state = {
            idx : [0,1,2,3,4,22,23],
            yunTopList : []
        }
    }

    async componentDidMount () {
        let yunTopList = [];
        let list = [];
        for ( let i = 0 ; i < this.state.idx.length ; i++){
             this.props.getTopList0({//新歌绑
                idx : this.state.idx[i]
            })
            list =await this.props.rank.topList0.playlist && this.props.rank.topList0.playlist
            // list.top = this.props.rank.topList0.playlist && this.props.rank.topList0.playlist.tracks.slice(0, 3)
            yunTopList.push(list)
            
        }
        console.log('yuntop',yunTopList)
        this.setState({
            yunTopList : yunTopList
        })
        // this.props.getTopList0({//新歌绑
        //     idx : 0
        // })
        // this.props.getTopList1({//热歌绑
        //     idx : 1
        // })
        // this.props.getTopList2({//原创绑
        //     idx : 2
        // })
        // this.props.getTopList3({//飙升绑
        //     idx : 3
        // })
        // this.props.getTopList4({//飙升绑
        //     idx : 4
        // })
        // this.props.getTopList22({//AGG音乐绑
        //     idx : 22
        // })
        // this.props.getTopListDetail()
        new BScroll(this.rankScroll,{//滚动实例化
            scrollY: true,
            click: true
        })
    }

    renderTopListItem = () => {
        return this.props.rank.topListDetail && this.props.rank.topListDetail.map( (item,index) => {
            return <TopListItem item = { item } key = { index }></TopListItem>
        })
    }

    render () {
        return (
            <Fragment>
                <div className="rank" ref = { el => this.rankScroll = el}>
                    <div className="toplist">
                        <ul>
                            
                        </ul>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default getRankStore({
    UIComponent: Rank
})
// export default Rank