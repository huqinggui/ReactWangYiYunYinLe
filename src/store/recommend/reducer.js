import * as type from './type'
import state from './state'

const reducer = (previousState = state , action) => {
  let newState = {
      ...previousState
  }

  switch ( action.type) {
        case type.BANNER_LIST:
            newState.bannerList = action.payload.data.banners;
            break;
        case type.PERSONALIZED:
            newState.personalized = action.payload.data.result;
            break;
        case type.TUIJIAN_SONG:
            newState.tuijianSong = action.payload.data.result;
            break;
        case type.PLAY_LIST:
            newState.playList.list = action.payload.arr;
            newState.playList.index = action.payload.index;
            newState.playList.length = action.payload.arr.length;
            newState.playList.imgs = action.payload.imgs;
            newState.playList.title = action.payload.title;
            newState.playList.name = action.payload.name;
            newState.playFlag = true
            break;
        case type.SONG_INFO:
            newState.songInfo = action.payload.data.data[0];
            break;
        case type.LYRIC:
            newState.lyric = action.payload.data
            break;
        default:
            break;
  }

  console.log('BANNER_LIST',newState)
 
  return newState
}

export default reducer 