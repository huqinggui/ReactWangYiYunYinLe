export const BANNER_LIST = 'BANNER_LIST' //首页轮播图
export const PERSONALIZED = 'PERSONALIZED' //推荐歌单
export const TUIJIAN_SONG = 'TUIJIAN_SONG' //推荐歌曲
export const PLAY_LIST = 'PLAY_LIST' //播放列表
export const SONG_INFO = 'SONG_INFO' //歌曲详情
export const LYRIC = 'LYRIC' //歌词