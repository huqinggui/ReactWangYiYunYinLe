const state = {
    bannerList : [], //轮播图
    personalized : [], //推荐歌单
    tuijianSong : [], //推荐歌曲
    playList : {
        list : [],
        index : '',
        length : Number,
        imgs : '',
        title : '',
        name : ''
    },
    playFlag : false,
    songInfo : [],
    lyric : null
}

export default state