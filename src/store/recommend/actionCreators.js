import * as type from './type'
import axios from 'axios'


const actionCreators = {
   getBannerList () {//获取banner列表
       return dispatch => {
          axios({
             method : 'get',
             url : '/wangyi/banner'
          })
          .then(data => {
            let action = {
               type : type.BANNER_LIST,
               payload : data
            }
            dispatch ( action )
          })
          .catch( error => {
             if( error ){
                throw error 
             }
          })
       }
   },
   getPersonalized () {//获取推荐歌单列表
      return dispatch => {
         axios({
            method : 'get',
            url : '/wangyi/personalized'
         })
         .then(data => {
            let action = {
               type : type.PERSONALIZED,
               payload : data
            }
            dispatch ( action )
         })
         .catch( error => {
            if( error ){
               throw error 
            }
         })
      }
   },
   getTuiJianSong () {//获取推荐歌单列表
      return dispatch => {
         axios({
            method : 'get',
            url : '/wangyi/personalized/newsong'
         })
         .then(data => {
            let action = {
               type : type.TUIJIAN_SONG,
               payload : data
            }
            dispatch ( action )
         })
         .catch( error => {
            if( error ){
               throw error 
            }
         })
      }
   },
   getPlayList ({arr,index,imgs,title,name}) {//获取播放列表
      let action = {
          type : type.PLAY_LIST,
          payload : {arr,index,imgs,title,name}
      }
      return action
  },
  getSongInfo ({id}) {//获取音乐url
   return dispatch => {
       axios({
           method : 'get',
           url : 'wangyi/song/url',
           params : {
               id
           }
       })
       .then( data => {
           let action = {
               type : type.SONG_INFO,
               payload : data
           }
           dispatch ( action )
       })
      }
   },
   getLyric ({id}) {
      return dispatch => {
          axios({
              method : 'get',
              url : 'wangyi/lyric',
              params : {
                  id
              }
          })
          .then( data => {
              let action = {
                  type : type.LYRIC,
                  payload : data
              }
              dispatch ( action )
          })
      }
  },
}

export default actionCreators