import * as type from './type'
import state from './state'

const reducer = ( previousState = state ,action ) => {

    let newState = {
        ...previousState
    }

    switch ( action.type ) {
        case type.SINGER_LIST:
            newState.singerList = action.payload.data.artists;
            break;
        default:
            break;
    }

    return newState
}

export default reducer 