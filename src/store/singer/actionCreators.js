import * as type from './type'
import axios from 'axios'

const actionCreators = {

    getSingerList ({ limit }) {
        return dispatch => {
            axios({
                method : 'get',
                url : '/wangyi/top/artists',
                params : {
                    limit
                }
            })
            .then ( data => {
                let action = {
                    type : type.SINGER_LIST,
                    payload : data
                }
                dispatch ( action )
            })
        }
    }
}

export default actionCreators