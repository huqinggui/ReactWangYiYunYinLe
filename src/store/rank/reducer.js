import * as type from './type'
import state from './state'

const reducer = ( previousState = state , action ) => {
    let newState = {
        ...previousState
    }

    switch ( action.type ) {
        case type.TOP_LIST0:
            newState.topList0 = action.payload.data
            break;
        case type.TOP_LIST1:
            newState.topList1 = action.payload.data
            break;
        case type.TOP_LIST2:
            newState.topList2 = action.payload.data
            break;
        case type.TOP_LIST3:
            newState.topList3 = action.payload.data
            break;
        case type.TOP_LIST4:
            newState.topList4 = action.payload.data
            break;
        case type.TOP_LIST22:
            newState.topList22 = action.payload.data
            break;
        case type.TOP_LIST_DETAIL:
            newState.topListDetail = action.payload.data.playlist
            break;
        case type.PLAY_LIST:
            newState.playList.list = action.payload.arr;
            newState.playList.index = action.payload.index;
            newState.playList.length = action.payload.arr.length;
            newState.playList.imgs = action.payload.imgs;
            newState.playList.title = action.payload.title;
            newState.playList.name= action.payload.name;
            newState.playFlag = true
            break;
        case type.LYRIC:
            newState.lyric = action.payload.data
            break;
        default:
            break;
    }

    // console.log('toplist',newState)

    return newState
}

export default reducer