import * as type from './type'
import axios from 'axios'

const actionCreators = {
    getTopList0 ({idx}) {
        return dispatch => {
            axios ({
                method : 'get',
                url : '/wangyi/top/list',
                params : {
                    idx
                }
            })
            .then( data => {
                let action = {
                    type : type.TOP_LIST0,
                    payload : data
                }
                dispatch ( action )
            })
        }
    },
    getTopList1 ({idx}) {
        return dispatch => {
            axios ({
                method : 'get',
                url : '/wangyi/top/list',
                params : {
                    idx
                }
            })
            .then( data => {
                let action = {
                    type : type.TOP_LIST1,
                    payload : data
                }
                dispatch ( action )
            })
        }
    },
    getTopList2 ({idx}) {
        return dispatch => {
            axios ({
                method : 'get',
                url : '/wangyi/top/list',
                params : {
                    idx
                }
            })
            .then( data => {
                let action = {
                    type : type.TOP_LIST2,
                    payload : data
                }
                dispatch ( action )
            })
        }
    },
    getTopList3 ({idx}) {
        return dispatch => {
            axios ({
                method : 'get',
                url : '/wangyi/top/list',
                params : {
                    idx
                }
            })
            .then( data => {
                let action = {
                    type : type.TOP_LIST3,
                    payload : data
                }
                dispatch ( action )
            })
        }
    },
    getTopList4 ({idx}) {
        return dispatch => {
            axios ({
                method : 'get',
                url : '/wangyi/top/list',
                params : {
                    idx
                }
            })
            .then( data => {
                let action = {
                    type : type.TOP_LIST4,
                    payload : data
                }
                dispatch ( action )
            })
        }
    },
    getTopList22 ({idx}) {
        return dispatch => {
            axios ({
                method : 'get',
                url : '/wangyi/top/list',
                params : {
                    idx
                }
            })
            .then( data => {
                let action = {
                    type : type.TOP_LIST22,
                    payload : data
                }
                dispatch ( action )
            })
        }
    },
    getTopListDetail ({id}) {
        return dispatch => {
            axios ({
                method : 'get',
                url : '/wangyi/playlist/detail',
                params : {
                    id
                }
            })
            .then( data => {
                let action = {
                    type : type.TOP_LIST_DETAIL,
                    payload : data
                }
                dispatch ( action )
            })
            .catch ( error => {
                if ( error ){
                    throw error
                }
            })
        }
    },
    getPlayList ({arr,index,imgs,title,name}) {//获取播放列表
        let action = {
            type : type.PLAY_LIST,
            payload : {arr,index,imgs,title,name}
        }
        return action
    },
    getLyric ({id}) {
        return dispatch => {
            axios({
                method : 'get',
                url : 'wangyi/lyric',
                params : {
                    id
                }
            })
            .then( data => {
                let action = {
                    type : type.LYRIC,
                    payload : data
                }
                dispatch ( action )
            })
        }
    },
}

export default actionCreators