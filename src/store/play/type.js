export const SONG_INFO = 'SONG_INFO' //歌曲详情
export const PLAY_LIST = 'PLAY_LIST' //播放列表
export const LYRIC = 'LYRIC' //歌词
export const PLAY_FLAG = 'PLAY_FLAG'
export const CHANGE_INDEX = 'CHANGE_INDEX'