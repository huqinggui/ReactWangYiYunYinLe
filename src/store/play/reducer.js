import * as type from './type'
import state from './state'

const reducer = ( previousState = state , action ) => {
    let newState = {
        ...previousState
    }

    switch ( action.type ) {
        case type.SONG_INFO:
            newState.songInfo = action.payload.data.data[0];
            break;
        case type.PLAY_LIST:
            newState.playList.list = action.payload.arr;
            newState.playList.index = action.payload.index;
            newState.playList.length = action.payload.arr.length;
            newState.playList.imgs = action.payload.imgs;
            newState.playList.title = action.payload.title;
            newState.playList.name= action.payload.name;
            newState.playFlag = true
            break;
        case type.LYRIC:
            newState.lyric = action.payload.data
            break;
        case type.PLAY_FLAG:
            newState.playFlag = false;
            break;
        case type.CHANGE_INDEX:
            newState.playList.index = action.payload
            break;
        default:
            break;
    }

    return newState
}

export default reducer