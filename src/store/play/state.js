const state = {
    songInfo : [],
    playList : {
        list : [],
        index : '',
        length : Number,
        imgs : '',
        title : '',
        name : ''
    },
    playFlag : false,
    lyric : null
}

export default state