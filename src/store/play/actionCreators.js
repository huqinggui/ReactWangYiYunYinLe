import * as type from './type'
import axios from 'axios';

const actionCreators = {
    getSongInfo ({id}) {//获取音乐url
        return dispatch => {
            axios({
                method : 'get',
                url : 'wangyi/song/url',
                params : {
                    id
                }
            })
            .then( data => {
                let action = {
                    type : type.SONG_INFO,
                    payload : data
                }
                dispatch ( action )
            })
        }
    },
    getLyric ({id}) {
        return dispatch => {
            axios({
                method : 'get',
                url : 'wangyi/lyric',
                params : {
                    id
                }
            })
            .then( data => {
                let action = {
                    type : type.LYRIC,
                    payload : data
                }
                dispatch ( action )
            })
        }
    },
    getPlayList ({arr,index,imgs,title,name}) {//获取播放列表
        let action = {
            type : type.PLAY_LIST,
            payload : {arr,index,imgs,title,name}
        }
        return action
    },
    changePlayFlag () { //控制play播放器的显示隐藏
        let action = {
            type : type.PLAY_FLAG,
        }
        return action
    },
    changeIndex (index) {
        let action = {
            type : type.CHANGE_INDEX,
            payload : index
        }
        return action
    }
}

export default actionCreators