import { combineReducers } from 'redux'

import recommend from './recommend/reducer'
import rank from './rank/reducer'
import singer from './singer/reducer.'
import playInfo from './play/reducer'

const reducer = combineReducers({
    recommend,rank,singer,playInfo
})

export default reducer