import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actionCreators from 'store/recommend/actionCreators';

const getRecommendStore = ( {UIComponent,dataType} ) => {

  return connect( state => {
    if( dataType ){
      return state[dataType]
    }else{
      return state
    }
  }, dispatch => {
    return bindActionCreators( actionCreators, dispatch )
  })( UIComponent )
}


export default getRecommendStore