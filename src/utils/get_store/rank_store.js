import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actionCreators from 'store/rank/actionCreators';

const getRankStore = ( {UIComponent,dataType} ) => {

  return connect( state => {
    if( dataType ){
      return state[dataType]
    }else{
      return state
    }
  }, dispatch => {
    return bindActionCreators( actionCreators, dispatch )
  })( UIComponent )
}


export default getRankStore