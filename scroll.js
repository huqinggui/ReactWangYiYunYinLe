import BScroll from 'better-scroll'
import http from 'Utils/http'
import {Toast} from 'mint-ui'
import _ from 'loadsh'

const scroll = ({
  el,
  arr,
  url,
  method,
  params,
  dataList,
  dataType
}) => {
  let count = 0 ;
  let bscroll = new BScroll(el,{
    probeType: 1,
    click: true,
    pullUpLoad: {
      threshold: 50
    }
  })

  bscroll.on('pullingUp',() => { //默认是关闭的，要想执行必须开启上拉加载 ,通过pullUpLoad 开启
    //数据请求

    if( count < arr.length ){
      //进行数据请求，继续拉
      http({
        url,
        method,
        params: {
          token: params.token,
          movieIds: arr[count].join(',')
        }
      }).then( res => {
        console.log(count)
        dataList.push( ...res.data[dataType] )
      })
    }

    bscroll.finishPullUp()
    if ( count == arr.length ){
      //到底了
      Toast({
        message: '大人到底了，不要在拉了~~',
        position: 'bottom',
        duration: 1000
      });
      return
    }

    count++
  })
}

export default scroll 